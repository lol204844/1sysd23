#include<stdio.h>

int n_chiffres(char *s) {
	int conteur = 0; 
	while (*s) {
		if (*s >= '0' && *s <= '9') {
			conteur++;
		}
		s++; 
	}
	return conteur;
}

int main(int argc, char *argv[]) { 
	char phrase[100]; 
	int cont = n_chiffres(argv[1]);
	printf("%d \n", cont);
	return 0;
}
