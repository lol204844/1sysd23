#include<stdio.h>


float inches2cm(float ie) {
	float conv;
	conv = ie * 2.54;
	return conv;
}

float cm2inches(float cm) {
	float convt;
	convt = cm / 2.54;
	return convt;
}
	
int main() {
	int val;
	char unite;
	float conv;
	printf("En quelle unité voulez vous convertir ? (c pour centimètre, i pour pouces) :");
	scanf("%c", &unite);
	if (unite == 'i') {
		printf("Entrez une valeur a convertir :");
		scanf("%d", &val);
		conv = cm2inches(val);
		printf("voici la valeur en pouces : %f \n", conv);
		return 0;
	} else {
		printf("Entrez une valeur a convertir :");
		scanf("%d", &val);
		conv = inches2cm(val);
		printf("voici la valeur en centimètre: %f \n", conv);
		return 0;
	}
}
